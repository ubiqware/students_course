package com.in28minutes.springboot.model;

import org.junit.Test;

import static org.junit.Assert.*;

public class CourseTest {
    Course course1 = new Course("foo","Foo Course", "Best course Ever", null);
    Course course2 = new Course("bar", "Bar Course", "Best course Ever, really", null);
    Course course3 = new Course("foo", "Baz Course", "Impostor course", null);

    @Test
    public void notEquals() {
        assertNotEquals(course1, course2);
    }

    @Test
    public void equals() {
        assertEquals(course1, course3);
    }

}